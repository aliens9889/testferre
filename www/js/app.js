// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var db;

angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.services', 'app.filters', 'ngCordova'])

  .run(function ($ionicPlatform, $window, $rootScope, $cordovaSQLite, $ionicLoading, $state, SQLStorageService, FerreAPIService) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      $rootScope.API_URL = 'http://192.168.1.5:8888/ferrepacifico/api/';
      // $rootScope.API_URL = 'http://ferrepacifico.com.ve/api/';

      try {
        db = $cordovaSQLite.openDB({ name: "ferrepacifico.db", location: 'default' });
      } catch (error) {
        alert(error);
      }

      loadTables($cordovaSQLite);

      $ionicLoading.show();

      try {
        console.log("Si entre");
        SQLStorageService.getUserCode().then(function (result) {
          console.debug('Getting user code');
          if (result.rows.length > 0 || result.rowsAffected > 0) {
            console.log("Si entre1");
            $ionicLoading.show();

            $rootScope.userCode = result.rows.item(0).codigo;
            SQLStorageService.getCustomers();
            SQLStorageService.getCustomersToDelete();
            SQLStorageService.getOrders();
            SQLStorageService.getOrdersToDelete();
            SQLStorageService.getChecks();

            window.resolveLocalFileSystemURL($window.cordova.file.applicationStorageDirectory + 'catalogo.json',
              function (fileEntry) {
              console.log("Si entre2");
                fileEntry.file(function (file) {
                  console.log("Si entre3");
                  var reader = new FileReader();

                  reader.onloadend = function (e) {
                    console.log("Si entre4");
                    var products = typeof this.result === 'string' ? JSON.parse(this.result) : this.result;
                    $rootScope.products = products;
                    console.info("Products loaded: " + products.length);
                    $ionicLoading.hide();
                  }

                  reader.readAsText(file);
                });

              }, function (error) {
                console.error("Error loading local products1");
                console.log(error);
                console.error(JSON.stringify(error));
                $ionicLoading.hide();
              }
            );

            $state.go('app.customers');
          }
        }, function (error) {
          console.error('Error getting userCode');
          console.error(error);
        });
      } catch (e) {
        console.error('Error');
        console.error(e.toString());
      } finally {
        $ionicLoading.hide();
      }

      $rootScope.cart = [];

      $rootScope.customers = [];

      $rootScope.customersToDelete = [];
      $rootScope.ordersToDelete = [];

      $rootScope.orders = [];
      $rootScope.checks = [];
      $rootScope.products = [];
    });
  });

function loadTables($cordovaSQLite) {
  // User
  $cordovaSQLite.execute(
    db,
    'CREATE TABLE IF NOT EXISTS usuarios (codigo varchar(50) NOT NULL)'
  )

  // Customers
  $cordovaSQLite.execute(
    db,
    'CREATE TABLE IF NOT EXISTS clientes (id int(11) NOT NULL, nuevo int(1) NOT NULL, editar int(1) NOT NULL, nombres varchar(100) NOT NULL, apellidos varchar(100) NOT NULL, rif varchar(50) NOT NULL, direccion_fiscal varchar(200) NOT NULL, direccion_entrega varchar(200) NOT NULL, telefono_movil varchar(100) NOT NULL, telefono_habitacion varchar(100) NOT NULL, fax varchar(100) NOT NULL, zona varchar(100) NOT NULL, codigo varchar(50) NOT NULL)'
  );

  // Customers to Delete
  $cordovaSQLite.execute(
    db,
    'CREATE TABLE IF NOT EXISTS clientes_a_eliminar (codigo varchar(50) NOT NULL)'
  )

  // Products
  $cordovaSQLite.execute(
    db,
    'CREATE TABLE IF NOT EXISTS productos (id int(11) NOT NULL, codigo varchar(50) NOT NULL, codigo_referencia varchar(100) DEFAULT NULL, nombre varchar(100) NOT NULL, precio varchar(100) NOT NULL DEFAULT "0", existencia int(11) NOT NULL DEFAULT "0", unidad varchar(200) NOT NULL DEFAULT "0", descripcion varchar(200) NOT NULL DEFAULT "0", imagen varchar(100000) NOT NULL DEFAULT "0")'
  )

  // Orders
  $cordovaSQLite.execute(
    db,
    'CREATE TABLE IF NOT EXISTS ordenes_de_compras (codigo varchar(50) NOT NULL, nuevo int(1) NOT NULL, codigo_cliente varchar(80) NOT NULL, nombres_cliente varchar(100) NOT NULL, apellidos_cliente varchar(100) NOT NULL, rif_cliente varchar(50) NOT NULL, fecha datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, productos varchar(10000000) NOT NULL)'
  )

  // Orders to Delete
  $cordovaSQLite.execute(
    db,
    'CREATE TABLE IF NOT EXISTS ordenes_a_eliminar (codigo varchar(50) NOT NULL)'
  )

  // Checks
  $cordovaSQLite.execute(
    db,
    'CREATE TABLE IF NOT EXISTS cheques_devueltos (id int(11) NOT NULL, fecha_rebote varchar(50) DEFAULT NULL, cliente varchar(200) DEFAULT NULL, banco varchar(200) DEFAULT NULL, numero varchar(200) DEFAULT NULL, fecha datetime NOT NULL DEFAULT CURRENT_TIMESTAMP)'
  )
}
