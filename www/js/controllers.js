angular.module('app.controllers', [])

  .controller('AppCtrl', function (
    $http, $scope, $rootScope, $state,
    $window, $ionicPopup, $cordovaNetwork,
    $ionicLoading, FerreAPIService,
    SQLStorageService, $cordovaFile) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.updateProducts = function () {
      $ionicLoading.show();

      var products = [];

      recursiveProductsInsertion($http, $cordovaFile, $ionicLoading, $window, $rootScope, $rootScope.API_URL, 0, 100, products);

      /*$http({
        method: 'get',
        url: $rootScope.API_URL + 'catalogo.json',
        headers: {
          'Content-Type': 'application/json'
        }
      }).success(function (data) {
        console.log(data.length);
      }).error(function (error, status) {
        console.error(JSON.stringify(error), status);
      }).finally(function () {
        $ionicLoading.hide();
      });*/
      /*
      if (!$cordovaNetwork.isOnline()) {
        var networkPopup = $ionicPopup.show({
          title: '<b>Sin conexión a internet</b>',
          template: 'Conéctese a internet e intentelo más tarde.',
          buttons: [
            {
              text: 'Ok',
              type: 'button-positive'
            }
          ]
        });

        setTimeout(function () {
          networkPopup.close();
        }, 1500);

      } else {
        $ionicLoading.show();

        FerreAPIService.getAllProducts().success(function (data) {
          if (data.mensaje) {
            console.log(JSON.stringify(data));
            $http({
              method: 'get',
              url: $rootScope.API_URL + 'catalogo.json',
            }).success(function (response) {
              console.log(response.length);
              $cordovaFile.writeFile($window.cordova.file.applicationStorageDirectory, 'catalogo.json', response, true).then(
                function (result) {
                  console.info('Successfully overwritten file');
                  console.info(JSON.stringify(result));

                  window.resolveLocalFileSystemURL($window.cordova.file.applicationStorageDirectory + 'catalogo.json',
                    function (fileEntry) {
                      fileEntry.file(function (file) {
                        var reader = new FileReader();

                        reader.onloadend = function (e) {
                          console.debug(typeof this.result);
                          var products = typeof this.result === 'string' ? JSON.parse(this.result) : this.result;
                          $rootScope.products = products
                          console.info("Products loaded: " + products.length);
                          $ionicLoading.hide();
                        }
                        reader.readAsText(file);
                      });

                    }, function (error) {
                      console.error("Error loading local products");
                      console.error(JSON.stringify(error));
                      $ionicLoading.hide();
                    }
                  );
                }, function (error) {
                  console.error('Error creating file');
                  console.error(JSON.stringify(error));
                }
              );
            }).error(function (error, status) {
              console.error('Error getting products');
              console.error(JSON.stringify(error));
              console.error(status);
            }).finally(function () {
              $ionicLoading.hide();
            });
          }
        });
      }*/
    }

    $scope.sync = function () {
      if (!$cordovaNetwork.isOnline()) {
        var networkPopup = $ionicPopup.show({
          title: '<b>Sin conexión a internet</b>',
          template: 'Conéctese a internet e intentelo más tarde.',
          buttons: [
            {
              text: 'Ok',
              type: 'button-positive'
            }
          ]
        });

        setTimeout(function () {
          networkPopup.close();
        }, 1500);

      } else {

        var customersError = false;
        var ordersError = false;

        $ionicLoading.show();
        var customers = _.filter($rootScope.customers, function (customer) {
          return customer.editar || customer.nuevo;
        });

        if (customers.length > 0) {
          customers.forEach(function (customer) {
            FerreAPIService.createOrEditCustomer(customer)
              .success(function (data) {
                if (typeof data === 'number') {
                  customersError = true;
                }
              })
              .error(function () {
                customersError = true;
              });
          });
        }

        var customersToDelete = $rootScope.customersToDelete ? $rootScope.customersToDelete : [];
        if (customersToDelete.length > 0) {
          customersToDelete.forEach(function (customerToDelete) {
            FerreAPIService.deleteCustomer(customerToDelete).success(function (data) {
              if (typeof data === 'number') {
                customersError = true;
              }
            }).error(function (error) {
              customersError = true;
            });
          });
        }

        var orders = _.filter($rootScope.orders, { nuevo: true });

        if (orders.length > 0) {
          orders.forEach(function (order) {
            FerreAPIService.createOrder(order);
          });
        }

        var ordersToDelete = $rootScope.ordersToDelete ? $rootScope.ordersToDelete : [];

        if (ordersToDelete.length > 0) {
          ordersToDelete.forEach(function (orderToDelete) {
            FerreAPIService.deleteOrder(orderToDelete);
          })
        }

        if (!customersError) {
          FerreAPIService.getCustomers();
        }

        if (!ordersError) {
          FerreAPIService.getOrders();
        }

        setTimeout(function () {
        }, 1000);

        FerreAPIService.getChecks();

        if (customersError || ordersError) {
          var syncPopup = $ionicPopup.show({
            title: '<b>Información de la sincronización</b>',
            template: 'Se presentaron algunos problemas sincronizando los datos, intente de nuevo.',
            buttons: [
              {
                text: 'Ok',
                type: 'button-positive'
              }
            ]
          });

          setTimeout(function () {
            syncPopup.close();
          }, 3000);
        }

        setTimeout(function () {
          $ionicLoading.hide();
        }, 1500);
      }
    };

    $scope.logout = function () {
      var logoutPopup = $ionicPopup.show({
        title: '<b>¿Está seguro que desea cerrar sesión?</b>',
        template: 'Si cierra sesión, todo el progreso guardado será eliminado.',
        buttons: [
          {
            text: 'Cancelar',
            type: 'button-light'
          },
          {
            text: 'Cerrar Sesión',
            type: 'button-positive',
            onTap: function (e) {
              delete $rootScope.userCode;
              $state.go('login');
              SQLStorageService.removeAllData();
            }
          }
        ]
      });
    }
  })
  .controller('LoginCtrl', function ($scope, $window, $rootScope, $cordovaNetwork, FerreAPIService, SQLStorageService, $ionicPopup, $ionicLoading, $state) {
    $scope.$on('$ionicView.beforeEnter', function (e) {
      if ($rootScope.userCode) {
        $state.go('app.customers');
      }
    });

    $scope.loginData = {};
    $scope.doLogin = function () {

      if (!$cordovaNetwork.isOnline()) {
        var networkPopup = $ionicPopup.show({
          title: '<b>Sin conexión a internet</b>',
          template: 'Conéctese a internet e intentelo más tarde.',
          buttons: [
            {
              text: 'Ok',
              type: 'button-positive'
            }
          ]
        });

        setTimeout(function () {
          networkPopup.close();
        }, 1500);

      } else {
        FerreAPIService.login($scope.loginData.username, $scope.loginData.password).success(function (data) {
          if (typeof data === 'object' && data.codigo !== undefined) {
            FerreAPIService.getCustomers();
            FerreAPIService.getOrders();
            FerreAPIService.getChecks();

            window.resolveLocalFileSystemURL($window.cordova.file.applicationStorageDirectory + 'catalogo.json',
              function (fileEntry) {
                fileEntry.file(function (file) {
                  var reader = new FileReader();

                  reader.onloadend = function (e) {
                    var products = typeof this.result === 'string' ? JSON.parse(this.result) : this.result;
                    $rootScope.products = products
                    console.info("Products loaded: " + products.length);
                    $ionicLoading.hide();
                  }

                  reader.readAsText(file);
                });

              }, function (error) {
                console.error("Error loading local products2");
                console.error(JSON.stringify(error));
                $ionicLoading.hide();
              }
            );

            $state.go('app.customers');
          } else {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Error al iniciar sesión',
              template: 'Verifique sus credenciales'
            });

            setTimeout(function () {
              alertPopup.close();
            }, 1500);
          }
        }).error(function (errorData) {
          $ionicLoading.hide();

          var alertPopup = $ionicPopup.alert({
            title: 'Error al iniciar sesión',
            template: 'Verifique sus credenciales'
          });

          setTimeout(function () {
            alertPopup.close();
          }, 1500);
        });
      }
    };
  })
  .controller('CustomersCtrl', function ($scope, $rootScope, $ionicPopup, $state, SQLStorageService) {
    $scope.create = function () {
      $rootScope.customerToEdit = undefined;
      $state.go('app.customer');
    }

    $scope.edit = function (code) {
      $rootScope.customerToEdit = _.find($rootScope.customers, { codigo: code });
      $state.go('app.customer');
    }

    $scope.sell = function (code) {
      $rootScope.currentCustomer = _.find($rootScope.customers, { codigo: code });
      $state.go('app.products');
    }

    $scope.delete = function (code) {
      var deletePopup = $ionicPopup.show({
        title: '<b>¿Está seguro que desea eliminar al cliente?</b>',
        buttons: [
          {
            text: 'Cancelar',
            type: 'button-light'
          },
          {
            text: '<b>Eliminar</b>',
            type: 'button-positive',
            onTap: function (e) {
              var customer = _.find($rootScope.customers, { codigo: code });

              if (!_.find($rootScope.customersToDelete, function (customerToDelete) {
                return customerToDelete === code;
              })) {
                if (!customer.nuevo) {
                  $rootScope.customersToDelete.push(customer.codigo);
                  SQLStorageService.saveCustomersToDelete($rootScope.customersToDelete);
                }
              }

              if (_.find($rootScope.customers, { codigo: code })) {
                _.remove($rootScope.customers, { codigo: code });
              }
            }
          }
        ]
      });
    }
  })

  .controller('CustomerCtrl', function ($scope, $rootScope, $state, $ionicHistory, SQLStorageService) {
    $scope.cliente = $rootScope.customerToEdit ? $rootScope.customerToEdit : {};

    $scope.cancel = function () {
      $rootScope.customerToEdit = undefined;
      $ionicHistory.goBack();
    };

    $scope.create = function () {
      var edit = $rootScope.customerToEdit ? true : false;

      if ($rootScope.customerToEdit ? $rootScope.customerToEdit.nuevo : false) {
        $scope.cliente.editar = false;
        $scope.cliente.nuevo = true;
      } else {
        $scope.cliente.editar = edit;
        $scope.cliente.nuevo = !edit;
      }

      if (edit) {
        _.remove($rootScope.customers, { codigo: $scope.cliente.codigo });
      }

      $scope.cliente.codigo = $scope.cliente.codigo ? $scope.cliente.codigo : moment().unix().toString();

      $rootScope.customers.push($scope.cliente);
      SQLStorageService.saveCustomers($rootScope.customers);

      $state.go('app.customers');
    }

    $scope.checkFields = function () {
      var nombres = $scope.cliente.nombres === undefined || $scope.cliente.nombres === '';
      var apellidos = $scope.cliente.apellidos === undefined || $scope.cliente.apellidos === '';
      var rif = $scope.cliente.rif === undefined || $scope.cliente.rif === '';
      var telefono_movil = $scope.cliente.telefono_movil === undefined || $scope.cliente.telefono_movil === '';

      return nombres || apellidos || rif || telefono_movil;
    }
  })
  .controller('ProductsCtrl', function ($scope, $rootScope, $state, $ionicModal) {

    $scope.search = false;

    $scope.$on('$ionicView.beforeEnter', function (e) {
      $scope.productList = $rootScope.products.slice(0, 15);
    });

    $scope.moreProductsCanBeLoaded = function () {
      var productList = $scope.productList ? $scope.productList : [];
      var lastTotal = productList.length;
      var newProducts = $rootScope.products.slice(lastTotal, lastTotal + 15);

      return newProducts.length > 0;
    };

    $scope.loadMoreProducts = function () {
      var lastTotal = $scope.productList.length;
      var newProducts = $rootScope.products.slice(lastTotal, lastTotal + 15);

      if (newProducts.length > 0) {
        $scope.productList = $scope.productList.concat(newProducts);
      }

      setTimeout(function () {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }, 1000);
    };

    $scope.$on('$ionicView.enter', function (e) {
      $scope.cart = $rootScope.cart;
    });

    $scope.goSearch = function () {
      $scope.search = true;
    }

    $scope.doSearch = function (searchText) {
      var lastProductList = $scope.productList;

      if (searchText && searchText !== '') {
        var search = _.filter($rootScope.products, function (product) {
          var productName = product.nombre.toLowerCase();
          return productName.indexOf(searchText.toLowerCase()) > -1;
        });
        $scope.productList = search ? search.slice(0, 15) : [];
      } else {
        $scope.products = lastProductList;
      }
    }

    $scope.cancelSearch = function () {
      $scope.productList = $rootScope.products.slice(0, 15);
      $scope.search = false;
    }

    $ionicModal.fromTemplateUrl('templates/product-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.openProductModal = function (productSelected) {
      $rootScope.productSelected = _.find($rootScope.products, { codigo: productSelected });
      $rootScope.quantity = 0;
      $scope.modal.show();
    }

    $scope.addProduct = function (quantity) {
      if ($rootScope.productSelected && quantity !== 0) {
        var oldProduct = _.find($rootScope.cart, { codigo: $rootScope.productSelected.codigo });
        if (oldProduct) {
          _.remove($rootScope.cart, { codigo: $rootScope.productSelected.codigo });
        }
        $rootScope.cart.push({
          codigo: $rootScope.productSelected.codigo,
          codigo_referencia: $rootScope.productSelected.codigo_referencia,
          nombre: $rootScope.productSelected.nombre,
          precio: $rootScope.productSelected.precio,
          existencia: $rootScope.productSelected.existencia,
          unidad: $rootScope.productSelected.unidad,
          descripcion: $rootScope.productSelected.descripcion,
          costo: parseFloat($rootScope.productSelected.precio),
          cantidad: quantity
        });
      }
      $scope.modal.hide();
      $rootScope.quantity = 0;
      $scope.cart = $rootScope.cart;
    }

    $scope.closeModal = function () {
      $rootScope.productSelected = null;
      $rootScope.quantity = 0;
      $scope.modal.hide();
    }

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $rootScope.productSelected = null;
      $rootScope.quantity = 0;
      $scope.modal.remove();
    });

    $scope.goToCart = function () {
      if ($rootScope.cart.length > 0) {
        $state.go('app.cart');
      }
    };
  })
  .controller('OrdersCtrl', function ($scope, $rootScope, $ionicPopup, SQLStorageService) {
    $scope.delete = function (code) {
      var orderPopup = $ionicPopup.show({
        title: '<b>¿Está seguro que desea eliminar la orden?</b>',
        buttons: [
          {
            text: 'Cancelar',
            type: 'button-light'
          },
          {
            text: '<b>Eliminar</b>',
            type: 'button-positive',
            onTap: function (e) {
              var order = _.find($rootScope.orders, { codigo: code });

              if (!_.find($rootScope.ordersToDelete, function (orderToDelete) {
                return orderToDelete === code;
              })) {
                if (!order.nuevo) {
                  $rootScope.ordersToDelete.push(order.codigo);
                  SQLStorageService.saveOrdersToDelete($rootScope.ordersToDelete);
                }
              }

              if (_.find($rootScope.orders, { codigo: code })) {
                _.remove($rootScope.orders, { codigo: code });
              }
            }
          }
        ]
      });
    }
  })
  .controller('ChecksCtrl', function ($scope, $rootScope, $state) {
  })
  .controller('CheckoutCtrl', function ($scope, $rootScope, $ionicHistory, $state, SQLStorageService) {
    $scope.processOrder = function (customerSelected) {
      var c = JSON.parse(customerSelected);

      var order = {
        apellidos_cliente: c.apellidos,
        nombres_cliente: c.nombres,
        rif_cliente: c.rif,
        codigo_cliente: c.codigo,
        productos: _.map($rootScope.cart, function (product) {
          product.codigo_cliente = c.codigo;
          return product;
        }),
        codigo: moment().unix().toString(),
        nuevo: true,
        fecha: moment().format('YYYY-MM-DD HH:mm:ss'),
        total: _.sumBy($rootScope.cart, function (producto) { return parseFloat(producto.precio) * parseInt(producto.cantidad); })
      };

      $rootScope.orders.push(order);

      SQLStorageService.saveOrders($rootScope.orders);
      $rootScope.cart = [];

      $ionicHistory.nextViewOptions({
        disableBack: true
      });

      $state.go('app.orders');
    };

    $scope.onSelect = function (customerSelected) {
      $scope.customerSelected = customerSelected;
    }
  })
  .controller('CartCtrl', function ($scope, $rootScope, $state, $ionicHistory, SQLStorageService) {
    $scope.cleanCart = function () {
      $rootScope.cart = [];
      $scope.cart = [];
      $state.go('app.products');
    }

    $scope.processOrder = function () {
      if ($rootScope.currentCustomer) {
        var order = {
          "apellidos_cliente": $rootScope.currentCustomer.apellidos,
          "nombres_cliente": $rootScope.currentCustomer.nombres,
          "rif_cliente": $rootScope.currentCustomer.rif,
          "codigo_cliente": $rootScope.currentCustomer.codigo,
          "productos": _.map($rootScope.cart, function (product) {
            product.codigo_cliente = $rootScope.currentCustomer.codigo;
            return product;
          }),
          "codigo": moment().unix(),
          "nuevo": true,
          "fecha": moment().format('YYYY-MM-DD HH:mm:ss'),
          "total": _.sumBy($rootScope.cart, function (producto) { return parseFloat(producto.precio) * parseInt(producto.cantidad); })
        };

        $rootScope.orders.push(order);
        SQLStorageService.saveOrders($rootScope.orders);

        $rootScope.cart = [];
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.orders');
      } else {
        $state.go('app.checkout');
      }
    }
  });

function recursiveProductsInsertion($http, $cordovaFile, $ionicLoading, $window, $rootScope, apiURL, from, to, products) {
  console.debug('Start from ' + from + ', to: ' + to);

  $http({
    method: 'get',
    url: apiURL + 'productos.php',
    params: {
      desde: from,
      hasta: to
    }
  }).success(function (data) {
    if (data.length > 0) {
      products = products.concat(data);
      from = to;
      to = to + 100;
      console.log("Products");
      console.log(products.length);
      recursiveProductsInsertion($http, $cordovaFile, $ionicLoading, $window, $rootScope, apiURL, from, to, products);
    } else {
      console.log('No more products');
      $cordovaFile.writeFile($window.cordova.file.applicationStorageDirectory, 'catalogo.json', JSON.stringify(products), true).then(
        function (result) {
          console.info('Successfully overwritten file');
          console.info(JSON.stringify(result));
          window.resolveLocalFileSystemURL($window.cordova.file.applicationStorageDirectory + 'catalogo.json',
            function (fileEntry) {
              fileEntry.file(function (file) {
                var reader = new FileReader();

                reader.onloadend = function (e) {
                  $rootScope.products = JSON.parse(this.result);
                  console.info("Products loaded: " + $rootScope.products.length);
                  $ionicLoading.hide();
                }
                reader.readAsText(file);
              });

            }, function (error) {
              console.error("Error loading local products3");
              console.error(JSON.stringify(error));
              $ionicLoading.hide();
            }
          );
        },
        function (error) {
          console.error("Error saving local products file");
          console.error(JSON.stringify(error));
          $ionicLoading.hide();
        }
      );
    }
  }).error(function (error) {
    console.error("Error getting products recursively");
    console.error(JSON.stringify(error));
    $ionicLoading.hide();
  });
}
