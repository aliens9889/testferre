angular.module('app.filters', [])
    .filter('totalPrice', function () {
        return function (value, arg1, arg2) {
            return parseInt(value.cantidad) * parseFloat(value.precio);
        }
    });