angular.module('app.routes', [])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })
            .state('app.customers', {
                url: '/customers',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/customers.html',
                        controller: 'CustomersCtrl'
                    }
                }
            })
            .state('app.customer', {
                url: '/customers/:customerCode',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/customer.html',
                        controller: 'CustomerCtrl'
                    }
                }
            })
            .state('app.products', {
                url: '/products',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/products.html',
                        controller: 'ProductsCtrl'
                    }
                }
            })
            .state('app.orders', {
                url: '/orders',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/orders.html',
                        controller: 'OrdersCtrl'
                    }
                }
            })
            .state('app.checks', {
                url: '/checks',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/checks.html',
                        controller: 'ChecksCtrl'
                    }
                }
            })
            .state('app.checkout', {
                url: '/checkout',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/checkout.html',
                        controller: 'CheckoutCtrl'
                    }
                }
            })
            .state('app.cart', {
                url: '/cart',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/cart.html',
                        controller: 'CartCtrl'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');
    });