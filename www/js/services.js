angular.module('app.services', [])
    .service('FerreAPIService', function ($http, $window, $ionicLoading, $rootScope, SQLStorageService) {
        this.login = function (name, pw) {
            $ionicLoading.show();
            var body = 'usuario=' + encodeURIComponent(name) + '&contrasena=' + encodeURIComponent(pw);

            return $http({
                method: 'post',
                url: $rootScope.API_URL + 'indentificar.php',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: body
            }).success(function (response, status) {
                if (response !== 403) {
                    $rootScope.userCode = response.codigo;
                    SQLStorageService.saveUserCode($rootScope.userCode);
                }
            }).error(function (error, status) {
                console.error(JSON.stringify(error));
                $ionicLoading.hide();
            });
        };

        this.getProducts = function () {
            $ionicLoading.show();
            recursiveInsertion($rootScope.API_URL, $http, SQLStorageService, 0, 30);
            SQLStorageService.getProducts();
        };

        this.getAllProducts = function () {
            return $http({
                method: 'get',
                url: $rootScope.API_URL + 'catalogo.php'
            });            
        }

        this.getLocalProducts = function (rootScope) {
            window.resolveLocalFileSystemURL($window.cordova.file.applicationStorageDirectory + 'catalogo.json',
                function (fileEntry) {
                    fileEntry.file(function (file) {
                        var reader = new FileReader();
                        
                        reader.onloadend = function (e) {
                            rootScope.products = JSON.parse(this.result);                   
                        }
                        
                        reader.readAsText(file);
                    });

                }, function (error) {
                    console.log(error);
                }
            );
            // return $http({
                // method: 'get',
                // url: $window.cordova.file.applicationStorageDirectory + 'catalogo.json'
                // url: 'files/catalogo.json'
            // });
        };

        this.getCustomers = function () {
            $ionicLoading.show();
            return $http({
                method: 'get',
                url: $rootScope.API_URL + 'clientes.php',
                headers: {
                    'Autorizacion': $rootScope.userCode
                }
            }).success(function (response, status) {
                if (typeof response !== 'number') {
                    $rootScope.customers = response;
                    SQLStorageService.saveCustomers(response);
                }
            }).error(function (error, status) {
                console.error('Error getting customers');
                console.error(JSON.stringify(error));
                $ionicLoading.hide();
            });
        };

        this.getChecks = function () {
            $ionicLoading.show();
            return $http({
                method: 'get',
                url: $rootScope.API_URL + 'cheques-devueltos.php',
                headers: {
                    'Autorizacion': $rootScope.userCode
                }
            }).success(function (response, status) {
                if (typeof response !== 'number') {
                    $rootScope.checks = response;
                    SQLStorageService.saveChecks(response);
                }
            }).error(function (error, status) {
                console.log('Error getting checks');
                console.error(JSON.stringify(error));
                $ionicLoading.hide();
            });
        };

        this.getOrders = function () {
            $ionicLoading.show();
            return $http({
                method: 'get',
                url: $rootScope.API_URL + 'ordenes-de-compra.php',
                headers: {
                    'Autorizacion': $rootScope.userCode
                }
            }).success(function (response, status) {
                if (typeof response !== 'number') {
                    var orders = response;
                    var newOrders = [];
                    _.forEach(orders, function (order) {
                        if (!_.find(newOrders, { codigo: order.codigo })) {
                            newOrders.push({
                                codigo_cliente: order.codigo_cliente,
                                nombres_cliente: order.nombres_cliente,
                                apellidos_cliente: order.apellidos_cliente,
                                rif_cliente: order.rif_cliente,
                                fecha: order.fecha,
                                codigo: order.codigo
                            });
                        }
                    });

                    _.forEach(newOrders, function (order) {
                        order.productos = _.map(_.filter(orders, { codigo: order.codigo }), function (o) {
                            return {
                                codigo_cliente: order.codigo_cliente,
                                codigo: moment().unix().toString(),
                                nombre: o.producto,
                                cantidad: parseInt(o.cantidad),
                                costo: parseFloat(o.costo)
                            };
                        });
                        order.total = _.sumBy(order.productos, function (producto) {
                            return producto.costo * producto.cantidad;
                        });
                    });

                    orders = newOrders;
                    $rootScope.orders = orders;
                    SQLStorageService.saveOrders(orders);
                }
            }).error(function (error, status) {
                console.log('Error getting orders');
                console.error(JSON.stringify(error));
                $ionicLoading.hide();
            });
        };

        this.createOrEditCustomer = function (customer) {
            var customerData = {
                codigo: customer.codigo,
                nombres: customer.nombres,
                apellidos: customer.apellidos,
                rif: customer.rif,
                telefono_movil: customer.telefono_movil,
                telefono_habitacion: customer.telefono_habitacion,
                fax: customer.fax,
                direccion_fiscal: customer.direccion_fiscal,
                direccion_entrega: customer.direccion_entrega,
                zona: customer.zona
            };

            if (customer.nuevo || !customer.editar) {
                delete customerData.codigo;
            }

            var keys = Object.keys(customerData);
            var body = '';

            keys.map(function (key) {
                if (customerData[key] !== undefined) {
                    body += key + '=' + encodeURIComponent(customerData[key]) + '&';
                }
            });

            body = body.slice(0, body.length - 2);

            return $http({
                method: 'post',
                url: $rootScope.API_URL + 'agregar-cliente.php',
                data: body,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Autorizacion': $rootScope.userCode
                }
            }).success(function (response, status) {
                console.debug('Create or edit customer');
                if (typeof response === 'object' && (response['nuevo cliente'] !== undefined || response['cliente actualizado'] !== undefined)) {
                    console.debug(JSON.stringify(response));
                } else {
                    console.error('Error creating/updating customer');
                }
            }).error(function (error, status) {
                console.error('Error creating/updating customer');
                console.error(JSON.stringify(error));
            });
        };

        this.deleteCustomer = function (customer) {
            var body = 'codigo=' + encodeURIComponent(customer);

            return $http({
                method: 'post',
                url: $rootScope.API_URL + 'eliminar-cliente.php',
                headers: {
                    'Autorizacion': $rootScope.userCode,
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: body
            }).success(function (response, status) {
                console.debug('Delete customer');
                console.debug(JSON.stringify(response));
                if (typeof response === 'object' && response['cliente eliminado'] !== undefined) {
                    _.remove($rootScope.customersToDelete, function (customerToDelete) {
                        return customerToDelete === customer;
                    });
                    if ($rootScope.customersToDelete.length > 0) {
                        SQLStorageService.saveCustomersToDelete($rootScope.customersToDelete);
                    }
                } else {
                    console.error('Error deleting customer');
                }
            }).error(function (error, status) {
                console.error(JSON.stringify(error));
            });
        };

        this.createOrder = function (order) {
            var codigo_cliente = encodeURIComponent(order.codigo_cliente);
            var carrito = encodeURIComponent(JSON.stringify(order.productos));

            var body = 'codigo_cliente=' + codigo_cliente + '&carrito=' + carrito;

            return $http({
                method: 'post',
                url: $rootScope.API_URL + 'agregar-orden-de-compra.php',
                data: body,
                headers: {
                    'Autorizacion': $rootScope.userCode,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (response, status) {
                if (response.indexOf("200") > -1) {
                    console.debug('Order created');
                    console.debug(JSON.stringify(response));
                } else {
                    console.error('Error creating order');
                    console.error(JSON.stringify(response));
                }
            }).error(function (error, status) {
                console.error(JSON.stringify(error));
            });
        }

        this.deleteOrder = function (order) {
            var body = 'codigo=' + encodeURIComponent(order);

            return $http({
                method: 'post',
                url: $rootScope.API_URL + 'eliminar-orden-de-compra.php',
                data: body,
                headers: {
                    'Autorizacion': $rootScope.userCode,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (response, status) {
                if (response.indexOf("200") > -1) {
                    _.remove($rootScope.ordersToDelete, function (orderToDelete) {
                        return orderToDelete === order;
                    });
                    console.debug('Customer deleted');
                    console.debug(JSON.stringify(response));
                    if ($rootScope.ordersToDelete.length > 0) {
                        SQLStorageService.saveOrdersToDelete($rootScope.ordersToDelete);
                    }
                } else {
                    console.error('Error deleting order');
                }
            }).error(function (error, status) {
                console.error(JSON.stringify(error));
            });
        };

        
    }).service('SQLStorageService', function ($rootScope, $cordovaSQLite) {
        //------------- User Code
        this.saveUserCode = function (userCode) {
            // Delete users
            $cordovaSQLite.execute(db, 'DELETE FROM usuarios');

            if (userCode) {
                $cordovaSQLite.execute(db, 'INSERT INTO usuarios (codigo) VALUES (?)', [userCode]).then(function (result) {
                    console.debug('userCode saved');
                    console.debug(JSON.stringify(result));
                }, function (error) {
                    console.error('Error saving userCode: ');
                    console.error(JSON.stringify(error));
                });
            }
        }

        this.getUserCode = function () {
            return $cordovaSQLite.execute(db, 'SELECT * FROM usuarios');
        }

        //------------- Products

        this.insertProducts = function (products) {
            var sql = 'INSERT INTO productos VALUES';

            products.forEach(function (producto, key) {
                sql += "('" + (key + 1) + "','" + producto.codigo + "','" + producto.codigo_referencia + "','" + producto.nombre + "','" + producto.precio + "','" + producto.existencia + "','" + producto.unidad + "','" + producto.descripcion + "','" + producto.imagen + "'),";
            });
            sql = sql.slice(0, sql.length - 1);

            $cordovaSQLite.execute(db, sql).then(function (result) {
                console.debug('Products inserted');
                console.debug(JSON.stringify(result));
            }, function (error) {
                console.error('Error inserting products');
                console.error(JSON.stringify(error));
            });
        }

        this.saveProducts = function (products) {
            // Delete old products
            $cordovaSQLite.execute(db, 'DELETE FROM productos');

            var sql = 'INSERT INTO productos VALUES';

            products.forEach(function (producto, key) {
                sql += "('" + (key + 1) + "','" + producto.codigo + "','" + producto.codigo_referencia + "','" + producto.nombre + "','" + producto.precio + "','" + producto.existencia + "','" + producto.unidad + "','" + producto.descripcion + "','" + producto.imagen + "'),";
            });
            sql = sql.slice(0, sql.length - 1);

            $cordovaSQLite.execute(db, sql).then(function (result) {
                console.debug('Products saved');
                console.debug(JSON.stringify(result));
            }, function (error) {
                console.error('Error saving products');
                console.error(JSON.stringify(error));
            });
        }

        this.getProducts = function () {
            $cordovaSQLite.execute(db, 'SELECT * FROM productos ORDER BY id ASC').then(
                function (result, foo) {
                    console.debug('Productos');
                    $rootScope.products = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        $rootScope.products.push(result.rows.item(i));
                    }
                },
                function (error) {
                    console.error('Error getting products');
                    console.error(JSON.stringify(error));
                }
            );
        }

        //------------- Customers
        this.getCustomers = function () {
            $cordovaSQLite.execute(db, 'SELECT * FROM clientes ORDER BY id ASC').then(
                function (result) {
                    console.debug('Clientes');
                    $rootScope.customers = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        var customer = result.rows.item(i);
                        customer.nuevo = Boolean(parseInt(customer.nuevo));
                        customer.editar = Boolean(parseInt(customer.editar));
                        $rootScope.customers.push(customer);
                    }
                },
                function (error) {
                    console.error('Error getting customers');
                    console.error(JSON.stringify(error));
                }
            )
        }

        this.saveCustomers = function (customers) {
            // Delete old customers
            $cordovaSQLite.execute(db, 'DELETE FROM clientes');

            var sql = 'INSERT INTO clientes VALUES';

            if (customers.length > 0) {
                customers.forEach(function (cliente, key) {
                    sql += "('" + (key + 1) + "','" + (cliente.nuevo ? 1 : 0) + "','" + (cliente.editar ? 1 : 0) + "','" + cliente.nombres + "','" + cliente.apellidos + "','" + cliente.rif + "','" + cliente.direccion_fiscal + "','" + cliente.direccion_entrega + "','" + cliente.telefono_movil + "','" + cliente.telefono_habitacion + "','" + cliente.fax + "','" + cliente.zona + "','" + cliente.codigo + "'),";
                });

                sql = sql.slice(0, sql.length - 1);

                $cordovaSQLite.execute(db, sql).then(function (result) {
                    console.debug('Customers saved');
                    console.debug(JSON.stringify(result));
                }, function (error) {
                    console.error('Error saving customers');
                    console.error(JSON.stringify(error));
                });

            }
        }

        this.getCustomersToDelete = function () {
            $cordovaSQLite.execute(db, 'SELECT * FROM clientes_a_eliminar').then(
                function (result) {
                    console.debug('Clientes a eliminar');
                    $rootScope.customersToDelete = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        $rootScope.customersToDelete.push(result.rows.item(i).codigo);
                    }
                },
                function (error) {
                    console.error('Error getting customers to delete');
                    console.error(JSON.stringify(error));
                }
            )
        }

        this.saveCustomersToDelete = function (customers) {
            // Delete old CustomersToDelete
            $cordovaSQLite.execute(db, 'DELETE FROM clientes_a_eliminar');

            var sql = 'INSERT INTO clientes_a_eliminar VALUES';

            if (customers.length > 0) {
                customers.forEach(function (cliente) {
                    sql += "('" + cliente + "'),";
                });

                sql = sql.slice(0, sql.length - 1);

                $cordovaSQLite.execute(db, sql).then(function (result) {
                    console.debug('CustomersToDelete saved');
                    console.debug(JSON.stringify(result));
                }, function (error) {
                    console.log('Error saving customers to delete');
                    console.error(JSON.stringify(error));
                });

            }
        }

        //------------- Checks
        this.getChecks = function () {
            $cordovaSQLite.execute(db, 'SELECT * FROM cheques_devueltos ORDER BY id ASC').then(
                function (result) {
                    console.debug('Cheques');
                    $rootScope.checks = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        $rootScope.checks.push(result.rows.item(i));
                    }
                },
                function (error) {
                    console.error('Error getting checks');
                    console.error(JSON.stringify(error));
                }
            )
        }

        this.saveChecks = function (checks) {
            // Delete old checks
            $cordovaSQLite.execute(db, 'DELETE FROM cheques_devueltos');

            var sql = 'INSERT INTO cheques_devueltos VALUES';

            if (checks.length > 0) {
                checks.forEach(function (check, key) {
                    sql += "('" + (key + 1) + "','" + check.fecha_rebote + "','" + check.cliente + "','" + check.banco + "','" + check.numero + "','" + check.fecha + "'),"
                });

                sql = sql.slice(0, sql.length - 1);

                $cordovaSQLite.execute(db, sql).then(
                    function (result) {
                        console.debug('Checks saved');
                        console.debug(JSON.stringify(result));
                    }, function (error) {
                        console.error('Error saving checks');
                        console.error(JSON.stringify(error));
                    });
            }
        }

        //------------- Orders
        this.getOrders = function () {
            var sql = "SELECT * FROM ordenes_de_compras ORDER BY fecha ASC";

            $cordovaSQLite.execute(db, sql).then(
                function (result) {
                    console.debug('Ordenes');

                    var orders = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        var order = result.rows.item(i);
                        order.productos = JSON.parse(order.productos);
                        order.total = _.sumBy(order.productos, function (producto) {
                            return producto.costo * producto.cantidad;
                        });
                        order.nuevo = Boolean(parseInt(order.nuevo));
                        orders.push(order);
                    }
                    $rootScope.orders = orders;
                },
                function (error) {
                    console.error('Error getting orders');
                    console.error(JSON.stringify(error));
                }
            )
        }

        this.saveOrders = function (orders) {
            // Delete old orders
            $cordovaSQLite.execute(db, 'DELETE FROM ordenes_de_compras');

            if (orders.length > 0) {
                var sql = 'INSERT INTO ordenes_de_compras VALUES';

                orders.forEach(function (order) {
                    sql += "('" + order.codigo + "','" + (order.nuevo ? 1 : 0) + "','" + order.codigo_cliente + "','" + order.nombres_cliente + "','" + order.apellidos_cliente + "','" + order.rif_cliente + "','" + order.fecha + "','" + JSON.stringify(order.productos) + "'),";
                });

                sql = sql.slice(0, sql.length - 1);

                $cordovaSQLite.execute(db, sql).then(
                    function (result) {
                        console.debug("Orders saved");
                        console.log(JSON.stringify(result));
                    }, function (error) {
                        console.error("Error saving error");
                        console.error(JSON.stringify(error));
                    }
                );
            }
        }

        this.getOrdersToDelete = function () {
            $cordovaSQLite.execute(db, 'SELECT * FROM ordenes_a_eliminar').then(
                function (result) {
                    console.debug('Ordenes a eliminar');
                    $rootScope.ordersToDelete = [];
                    for (var i = 0; i < result.rows.length; i++) {
                        $rootScope.ordersToDelete.push(result.rows.item(i));
                    }
                },
                function (error) {
                    console.error('Error getting orders to delete');
                    console.error(JSON.stringify(error));
                }
            )
        }

        this.saveOrdersToDelete = function (orders) {
            // Delete old CustomersToDelete
            $cordovaSQLite.execute(db, 'DELETE FROM ordenes_a_eliminar');

            var sql = 'INSERT INTO ordenes_a_eliminar VALUES';

            if (orders.length > 0) {
                orders.forEach(function (orden) {
                    sql += "('" + orden + "'),";
                });

                sql = sql.slice(0, sql.length - 1);

                $cordovaSQLite.execute(db, sql).then(function (result) {
                    console.debug('CustomersToDelete saved');
                    console.debug(JSON.stringify(result));
                }, function (error) {
                    console.error('Error saving customers to delete');
                    console.error(JSON.stringify(error));
                });
            }

        }

        this.removeAllData = function () {
            console.log('Remove data');

            try {
                $cordovaSQLite.execute(db, 'DELETE FROM usuarios').then(
                    function (result) {
                        console.info('Users removed');
                    },
                    function (error) {
                        console.warn(JSON.stringify(error));
                    }
                );
                $cordovaSQLite.execute(db, 'DELETE FROM ordenes_a_eliminar').then(
                    function (result) {
                        console.info('Orders to delete removed');
                    },
                    function (error) {
                        console.warn(JSON.stringify(error));
                    }
                );
                $cordovaSQLite.execute(db, 'DELETE FROM ordenes_de_compras').then(
                    function (result) {
                        console.info('Orders removed');
                    },
                    function (error) {
                        console.warn(JSON.stringify(error));
                    }
                );
                $cordovaSQLite.execute(db, 'DELETE FROM clientes_a_eliminar').then(
                    function (result) {
                        console.info('Customers to delete removed');
                    },
                    function (error) {
                        console.warn(JSON.stringify(error));
                    }
                );
                $cordovaSQLite.execute(db, 'DELETE FROM clientes').then(
                    function (result) {
                        console.info('Customers removed');
                    },
                    function (error) {
                        console.warn(JSON.stringify(error));
                    }
                );
            } catch (error) {
                /* empty */
                console.warn('Error removing data');
                console.warn(error);
            }
        }

        this.dropProducts = function () {
            $cordovaSQLite.execute(db, 'DELETE FROM productos');
        }

    });

function recursiveInsertion(url, $http, SQLStorageService, from, to) {
    console.info('Insert from ' + from + ', to: ' + to);
    
    $http({
        method: 'get',
        url: url + 'productos.php',
        params: {
            desde: from,
            hasta: to
        }
    }).success(function (response) {        
        if (typeof response !== 'number') {
            if (response.length > 0) {
                SQLStorageService.insertProducts(response);
                from = to;
                to += 30;
                recursiveInsertion(url, $http, SQLStorageService, from, to);
            } else {
                console.info('Products inserted successfully');
            }
        }
    }).error(function (error, status) {
        console.error('Error getting products');
        console.error(JSON.stringify(error));        
    });    
}